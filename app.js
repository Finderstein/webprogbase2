const express = require('express');
const mustache = require('mustache-express');
const path = require('path');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const mongoose = require('mongoose');

require('./models/telegram');

const app = express();

const User = require("./models/user");

const config = require('./config');

const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(busboyBodyParser({ limit: '5mb' }));

app.use(express.static('public'));

// new middleware
app.use(cookieParser());
app.use(session({
	secret: config.SecretString,
	resave: false,
	saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

function sha512(password, salt){
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    User.getById(id)
        .then(user =>
        {
            if (!user) done("No user", null);
            else done(null, user);
        })
        .catch(err => done(err, null));
});

passport.use(new LocalStrategy(
    function(username, password, done) {
        User.findByLoginAndPas(username, sha512(password, config.ServerSalt).passwordHash)
            .then(user =>
            {
                if (!user || user.isDisabled === true) return done(null, false);
                return done(null, user);
            });
    }
));

const PORT = config.ServerPort;
const databaseUrl = config.DatabaseUrl;
const connectOptions = { useNewUrlParser: true};

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(PORT, function() { console.log('Server is ready'); }))
    .catch(err => console.log(`Start error ${err}`));

app.get('/auth/login', function(req, res)
{
    res.render('login', { err: req.query.error });
});

app.post('/auth/login',
    passport.authenticate('local', { failureRedirect: '/auth/login?error=Invalid+username+or+password' }),  
    (req, res) => res.redirect('/'));

// вихід із сесії
app.get('/auth/logout', 
(req, res) => {
    req.logout();   
    res.redirect('/');
});
    
// usage
app.get('/', function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";
    res.render('index', { user: req.user, admin: admin });
});

app.get('/about', function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";
    res.render('about', { user: req.user, admin: admin });
});


const usersRouter = require('./routes/users');
const notesRouter = require('./routes/notes');
const listsRouter = require('./routes/lists');
const authRouter = require('./routes/auth');
const developerRouter = require('./routes/developer');
const apiRouter = require('./routes/api');
app.use('/api/v1', apiRouter);
app.use('/developer/v1', developerRouter);
app.use('/lists', listsRouter);
app.use('/', notesRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);

app.use(function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    res.render('404', { user: req.user, admin: admin });
});